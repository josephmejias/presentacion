/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaOlimpico.presentacion.tl;

import java.io.IOException;
import java.time.LocalDate;
import medallistaOlimpico.logicanegocios.bl.logic.Gestor;
import medallistaOlimpico.presentacion.ui.UI;

public class Controlador {

    final private Gestor gestor;
    final private UI ui;

    public Controlador() {
        gestor = new Gestor();
        ui = new UI();
    }

    public void ejecutar() throws IOException {

        boolean ward = true;
        while (true) {
            if (ward) {
                ui.showMainMenu();
            } else {
                ui.print("Opción inválida");
            }
            int opcion = ui.readInt();
            switch (opcion) {
                case 1:
                    registrarAtleta();
                    break;
                case 2:
                    registrarReto();
                    break;
                case 3:
                    registrarHito();
                    break;
                case 4:
                    listarAtletas();
                    break;
                case 5:
                    listarRetos();
                    break;
                case 6:
                    listarHitos();
                    break;
                case 7:
                    System.exit(0);
                    break;
                default:
                    ward = false;
                    break;

            }

        }
    }

    private void registrarAtleta() throws IOException {

        ui.print("Registrar Atleta:"
                + "\nPor favor ingrese el nombre");
        String nombreAtleta = ui.readString();
        ui.print("Por favor ingrese los apellidos");
        String apellidosAtleta = ui.readString();
        ui.print("Por favor ingrese el genero");
        String generoAtleta = ui.readString();
        ui.print("Por favor ingrese la cedula");
        String cedulaAtleta = ui.readString();
        ui.print("Por favor ingrese el correo");
        String correoAtleta = ui.readString();
        ui.print("Por favor ingrese la contraseña");
        String claveAtleta = ui.readString();
        ui.print("Por favor ingrese la fecha de nacimiento (Formato: 2020-02-02)");
        String fechaNacimientoAtleta = ui.readString();
        gestor.registrarAtleta(LocalDate.parse(fechaNacimientoAtleta), null, generoAtleta, 0, nombreAtleta, "", apellidosAtleta, cedulaAtleta, null, correoAtleta, claveAtleta);
    }

    private void registrarHito() throws IOException {
        ui.print("Registrar Atleta:"
                + "\nPor favor ingrese el numero del hito");
        int numHito = ui.readInt();
        ui.print("Por favor ingrese la descripcion del hito");
        String generoAtleta = ui.readString();
        gestor.registrarHito(0, null, numHito, "", generoAtleta, null);
    }

    private void registrarReto() throws IOException {
        ui.print("Registrar Reto:"
                + "\nPor favor ingrese el nombre");
        String nombreReto = ui.readString();
        ui.print("Por favor ingrese la descripcion del reto");
        String descripcionReto = ui.readString();
        ui.print("Por favor ingrese el codigo de acceso al reto");
        String codigoAcceso = ui.readString();
        gestor.registrarReto(0, nombreReto, descripcionReto, null, null, null, null, codigoAcceso);
    }

    private void listarAtletas() {
        gestor.listarAtletas().forEach(e -> {
            ui.print(e.toString());
        });
    }

    private void listarRetos() {
        gestor.listarRetos().forEach(e -> {
            ui.print(e.toString());
        });
    }

    private void listarHitos() {
        gestor.listarHitos().forEach(e -> {
            ui.print(e.toString());
        });
    }

}
