/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaOlimpico.presentacion.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Scanner;

public class UI {

    final private PrintStream out = System.out;
    final private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public UI() {
    }
    
    public void showMainMenu(){
        System.out.println("===Menu==="
                + "\n Selecciona: \n"
                + "1- Registrar Atleta\n"
                + "2- Registrar Reto\n"
                + "3- Registrar Hito\n"
                + "4- Listar Atletas\n"
                + "5- Listas Retos\n"
                + "6- Listar Hitos\n"
                + "7- Salir\n");
    }
    
    public void print(String text){
        out.println(text);
    }

    public String readString() throws IOException {
        return in.readLine();
    }

    
    public int readInt() throws IOException {
        return Integer.parseInt(in.readLine());
    }

}
